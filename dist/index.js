"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = importer;

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _findParentDir = require("find-parent-dir");

var _findParentDir2 = _interopRequireDefault(_findParentDir);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function resolve(targetUrl, source) {
    var packageRoot = _findParentDir2.default.sync(source, 'node_modules');

    if (!packageRoot) return null;

    var filePath = _path2.default.resolve(packageRoot, 'node_modules', targetUrl);

    if (_fs2.default.existsSync(_path2.default.dirname(filePath))) return filePath;

    return resolve(targetUrl, _path2.default.dirname(packageRoot));
}

function importer(url, prev, done) {
    if (url[0] === '~') url = resolve(url.substr(1), prev);

    return { file: url };
};