import path from "path"
import findParentDir from "find-parent-dir"
import fs from "fs"

function resolve(targetUrl, source) {
    const packageRoot = findParentDir.sync(source, 'node_modules');

    if (!packageRoot)
        return null;

    const filePath = path.resolve(packageRoot, 'node_modules', targetUrl);

    if (fs.existsSync(path.dirname(filePath)))
        return filePath;

    return resolve(targetUrl, path.dirname(packageRoot));
}

export default function importer (url, prev, done) {
    if (url[ 0 ] === '~')
        url = resolve(url.substr(1), prev);

    return { file: url };
};